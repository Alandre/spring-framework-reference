[[mvc-exceptionhandlers]]
=== Handling exceptions



[[mvc-exceptionhandlers-resolver]]
==== HandlerExceptionResolver

Spring `HandlerExceptionResolver` implementations deal with unexpected exceptions that
occur during controller execution. A `HandlerExceptionResolver` somewhat resembles the
exception mappings you can define in the web application descriptor `web.xml`. However,
they provide a more flexible way to do so. For example they provide information about
which handler was executing when the exception was thrown. Furthermore, a programmatic
way of handling exceptions gives you more options for responding appropriately before
the request is forwarded to another URL (the same end result as when you use the Servlet
specific exception mappings).

Besides implementing the `HandlerExceptionResolver` interface, which is only a matter of
implementing the `resolveException(Exception, Handler)` method and returning a
`ModelAndView`, you may also use the provided `SimpleMappingExceptionResolver` or create
`@ExceptionHandler` methods. The `SimpleMappingExceptionResolver` enables you to take
the class name of any exception that might be thrown and map it to a view name. This is
functionally equivalent to the exception mapping feature from the Servlet API, but it is
also possible to implement more finely grained mappings of exceptions from different
handlers. The `@ExceptionHandler` annotation on the other hand can be used on methods
that should be invoked to handle an exception. Such methods may be defined locally
within an `@Controller` or may apply to many `@Controller` classes when defined within an
`@ControllerAdvice` class. The following sections explain this in more detail.



[[mvc-ann-exceptionhandler]]
==== @ExceptionHandler

The `HandlerExceptionResolver` interface and the `SimpleMappingExceptionResolver`
implementations allow you to map Exceptions to specific views declaratively along with
some optional Java logic before forwarding to those views. However, in some cases,
especially when relying on `@ResponseBody` methods rather than on view resolution, it
may be more convenient to directly set the status of the response and optionally write
error content to the body of the response.

You can do that with `@ExceptionHandler` methods. When declared within a controller such
methods apply to exceptions raised by `@RequestMapping` methods of that contoroller (or
any of its sub-classes). You can also declare an `@ExceptionHandler` method within an
`@ControllerAdvice` class in which case it handles exceptions from `@RequestMapping`
methods from many controllers. Below is an example of a controller-local
`@ExceptionHandler` method:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Controller
	public class SimpleController {

		// @RequestMapping methods omitted ...

		@ExceptionHandler(IOException.class)
		public ResponseEntity<String> handleIOException(IOException ex) {
			// prepare responseEntity
			return responseEntity;
		}

	}
----

The `@ExceptionHandler` value can be set to an array of Exception types. If an exception
is thrown that matches one of the types in the list, then the method annotated with the
matching `@ExceptionHandler` will be invoked. If the annotation value is not set then
the exception types listed as method arguments are used.

Much like standard controller methods annotated with a `@RequestMapping` annotation, the
method arguments and return values of `@ExceptionHandler` methods can be flexible. For
example, the `HttpServletRequest` can be accessed in Servlet environments and the
`PortletRequest` in Portlet environments. The return type can be a `String`, which is
interpreted as a view name, a `ModelAndView` object, a `ResponseEntity`, or you can also
add the `@ResponseBody` to have the method return value converted with message
converters and written to the response stream.



